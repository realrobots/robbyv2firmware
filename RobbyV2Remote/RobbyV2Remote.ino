/*
  REALROBOTS.net
  Author: Jacob Wilkinson

  Robby v2 Transmitter Firmware
  This code runs on the Remote ESP Controller and responds to input requests
  from Robby the Robot v2

  ssid must match ssid on Robby firmware

  // ESPAsyncWebServer directory must be moved to Arduino/libraries/
*/

#include "WiFi.h"
#include <ESPAsyncWebServer.h>
#include <driver/adc.h>

bool printInputs = true;

int inputPins[] = {35, 34, 27, 32, 33, 25, 13};
int inputs[] = {0, 0, 0, 0, 0, 0, 0};

// Set your access point network credentials
const char *ssid = "robby8";
const char *password = "123456789";

long lastUpdate = 0;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

String readInputs()
{
  digitalWrite(2, !digitalRead(2));
  String data = "";
  for (int i = 0; i < 7; i++)
  {
    data = data + String(inputs[i]);
    if (i < 6)
      data = data + ",";
  }
  return data;
}

void setup()
{
  // Serial port for debugging purposes
  Serial.begin(115200);
  Serial.println();

  for (int i = 2; i < 7; i++)
  {
    pinMode(inputPins[i], INPUT_PULLUP);
  }
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_MAX);
  adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_MAX);

  pinMode(2, OUTPUT);

  // Setting the ESP as an access point
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.on("/inputs", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/plain", readInputs().c_str()); });

  // Start server
  server.begin();
}

void loop()
{
  if (millis() - lastUpdate > 50)
  {

    inputs[1] = adc1_get_raw(ADC1_CHANNEL_6);
    inputs[0] = adc1_get_raw(ADC1_CHANNEL_7);
    for (int i = 2; i < 7; i++)
    {
      inputs[i] = digitalRead(inputPins[i]);
    }

    if (printInputs)
    {
      for (int i = 0; i < 7; i++)
      {

        Serial.print(inputs[i]);

        Serial.print("\t");
      }
      Serial.println();
    }

    int x = -map(inputs[0], 0, 4096, -100, 100);
    if (x > -25 && x < 25)
      x = 0;
    int y = map(inputs[1], 0, 4096, -100, 100);
    if (y > -25 && y < 25)
      y = 0;

    int l = (x + y);
    int r = (y - x);

    if (l > 100)
    {
      int diff = l - 100;
      l = l - diff;
      r += diff / 2;
    }

    if (r > 100)
    {
      int diff = r - 100;
      r = r - diff;
      l += diff / 2;
    }

    // if (l < -100)
    // {
    //   int diff = -l - 100;
    //   l = l + diff;
    //   r -= diff / 2;
    // }
    int temp = 0;

    if (r < -100)
    {
      int diff = -r - 100;
      r = r + diff;
      l -= diff / 2;
    }

    if (l < -100)
    {
      int diff = -l - 100;
      l = l + diff;
      r -= diff / 2;
    }

    r = map(r, -100, 100, -255, 255);
    l = map(l, -100, 100, -255, 255);

    inputs[0] = l;
    inputs[1] = r;

    Serial.print(inputs[0]);

    Serial.print("\t");
    Serial.print(inputs[1]);
    Serial.println();
    // Serial.print(l);
    // Serial.print("\t");
    // Serial.println(r);

    lastUpdate = millis();
  }
}
