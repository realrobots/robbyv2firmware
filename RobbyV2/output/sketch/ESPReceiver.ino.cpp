#include <Arduino.h>
#line 1 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-client-server-wi-fi/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <WiFi.h>
#include <HTTPClient.h>
#include <math.h>

#define left_en  14
#define left_a   27
#define left_b   26
#define right_a  25
#define right_b  33
#define right_en 32 

const char* ssid = "robby6";
const char* password = "123456789";

//Your IP address or domain name with URL path
const char* serverNameInputs = "http://192.168.4.1/inputs";


String inputString;
bool printValues = false;

int inputs[7];

long lastConnect = 0;
unsigned long previousMillis = 0;
const long interval = 100; 

// setting PWM properties
const int freq = 5000;
const int pwmChannelLeft = 0;
const int pwmChannelRight = 1;
const int resolution = 8;

#line 45 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
void setup();
#line 69 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
void connect();
#line 82 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
void loop();
#line 138 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
void SetMotor(int l, int r);
#line 175 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
String httpGETRequest(const char* serverName);
#line 202 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
int getValue(String data, char separator, int index);
#line 45 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
void setup() {
  Serial.begin(115200);
  
  //pinMode(left_en, OUTPUT);
  pinMode(left_a, OUTPUT);
  pinMode(left_b, OUTPUT);
  //pinMode(right_en, OUTPUT);
  pinMode(right_a, OUTPUT);
  pinMode(right_b, OUTPUT);
  // digitalWrite(right_en, 1);
  // digitalWrite(left_en, 1);

  ledcSetup(pwmChannelLeft, freq, resolution);
  ledcAttachPin(left_en, pwmChannelLeft);
  ledcSetup(pwmChannelRight, freq, resolution);
  ledcAttachPin(right_en, pwmChannelRight);

  ledcWrite(pwmChannelLeft, 0);
  ledcWrite(pwmChannelRight, 0);

  pinMode(2, OUTPUT);
  connect(); 
}  

void connect(){
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

}

void loop() {
  unsigned long currentMillis = millis();
  
  if(currentMillis - previousMillis >= interval) {
     // Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED ){ 
      inputString = httpGETRequest(serverNameInputs);
      if (printValues) Serial.println("InputString: " + inputString);
      //Serial.println(getValue(inputString, ',', 1));
      for (int i = 0; i < 7; i++){
        inputs[i] = getValue(inputString, ',', i);
        if (printValues){
        Serial.print(i);
        Serial.print(":");
        Serial.print(inputs[i]);
        Serial.print("\t");
        } 
      }

      SetMotor(inputs[1],inputs[0]);
      lastConnect = millis();

      // if (inputs[1] < 4096/2 - 500){
      //   int pwr = map(inputs[1], 0, 4096/2-500, -255, -150);
      //   SetMotor(pwr, pwr);
      // } else if (inputs[1] > 4096/2 + 500){
      //   int pwr = map(inputs[1], 4096/2+500, 4096, 150, 255);
      //   SetMotor(pwr, pwr);
      // } else {
      //   SetMotor(0, 0);
      // }

      digitalWrite(2, !digitalRead(2));
      if (printValues) Serial.println();
      // save the last HTTP GET Request
      previousMillis = currentMillis;
    }
    else {
      Serial.println("WiFi Disconnected");
      SetMotor(0,0);
      connect();
    }
  }


  // Serial.println("FWD");
  // SetMotor(1, 1);
  // delay(2000);
  // Serial.println("BACK");
  // SetMotor(-1, -1);
  // delay(2000);
  // Serial.println("STOP");
  // SetMotor(0, 0);
  // delay(2000);
}

void SetMotor(int l, int r){
  // Serial.print(l);
  // Serial.print("\t");
  // Serial.println(r);
  //return;
  
  
  
  
  if (l > 0){
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(left_a, 0);
    digitalWrite(left_b, 1);
  } else if (l < 0){
    ledcWrite(pwmChannelLeft, -l);
    digitalWrite(left_a, 1);
    digitalWrite(left_b, 0);
  } else {
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(left_a, 0);
    digitalWrite(left_b, 0);
  }
  if (r > 0){
    ledcWrite(pwmChannelRight, r);
    digitalWrite(right_a, 1);
    digitalWrite(right_b, 0);
  } else if (r < 0){
    ledcWrite(pwmChannelRight, -r);
    digitalWrite(right_a, 0);
    digitalWrite(right_b, 1);
  } else {
    ledcWrite(pwmChannelRight, r);
    digitalWrite(right_a, 0);
    digitalWrite(right_b, 0);
  }
}

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "--"; 
  
  if (httpResponseCode>0) {
    //Serial.print("HTTP Response code: ");
    //Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

int getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return (found > index ? data.substring(strIndex[0], strIndex[1]) : "").toInt();
     
}
