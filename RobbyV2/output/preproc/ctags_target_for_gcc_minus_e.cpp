# 1 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-client-server-wi-fi/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

# 13 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino" 2
# 14 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino" 2
# 15 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino" 2
# 23 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"

# 23 "/home/jake/Documents/ESPReceiver/ESPReceiver.ino"
const char* ssid = "robby6";
const char* password = "123456789";

//Your IP address or domain name with URL path
const char* serverNameInputs = "http://192.168.4.1/inputs";


String inputString;
bool printValues = false;

int inputs[7];

long lastConnect = 0;
unsigned long previousMillis = 0;
const long interval = 100;

// setting PWM properties
const int freq = 5000;
const int pwmChannelLeft = 0;
const int pwmChannelRight = 1;
const int resolution = 8;

void setup() {
  Serial.begin(115200);

  //pinMode(left_en, OUTPUT);
  pinMode(27, 0x02);
  pinMode(26, 0x02);
  //pinMode(right_en, OUTPUT);
  pinMode(25, 0x02);
  pinMode(33, 0x02);
  // digitalWrite(right_en, 1);
  // digitalWrite(left_en, 1);

  ledcSetup(pwmChannelLeft, freq, resolution);
  ledcAttachPin(14, pwmChannelLeft);
  ledcSetup(pwmChannelRight, freq, resolution);
  ledcAttachPin(32, pwmChannelRight);

  ledcWrite(pwmChannelLeft, 0);
  ledcWrite(pwmChannelRight, 0);

  pinMode(2, 0x02);
  connect();
}

void connect(){
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

}

void loop() {
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis >= interval) {
     // Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED ){
      inputString = httpGETRequest(serverNameInputs);
      if (printValues) Serial.println("InputString: " + inputString);
      //Serial.println(getValue(inputString, ',', 1));
      for (int i = 0; i < 7; i++){
        inputs[i] = getValue(inputString, ',', i);
        if (printValues){
        Serial.print(i);
        Serial.print(":");
        Serial.print(inputs[i]);
        Serial.print("\t");
        }
      }

      SetMotor(inputs[1],inputs[0]);
      lastConnect = millis();

      // if (inputs[1] < 4096/2 - 500){
      //   int pwr = map(inputs[1], 0, 4096/2-500, -255, -150);
      //   SetMotor(pwr, pwr);
      // } else if (inputs[1] > 4096/2 + 500){
      //   int pwr = map(inputs[1], 4096/2+500, 4096, 150, 255);
      //   SetMotor(pwr, pwr);
      // } else {
      //   SetMotor(0, 0);
      // }

      digitalWrite(2, !digitalRead(2));
      if (printValues) Serial.println();
      // save the last HTTP GET Request
      previousMillis = currentMillis;
    }
    else {
      Serial.println("WiFi Disconnected");
      SetMotor(0,0);
      connect();
    }
  }


  // Serial.println("FWD");
  // SetMotor(1, 1);
  // delay(2000);
  // Serial.println("BACK");
  // SetMotor(-1, -1);
  // delay(2000);
  // Serial.println("STOP");
  // SetMotor(0, 0);
  // delay(2000);
}

void SetMotor(int l, int r){
  // Serial.print(l);
  // Serial.print("\t");
  // Serial.println(r);
  //return;




  if (l > 0){
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(27, 0);
    digitalWrite(26, 1);
  } else if (l < 0){
    ledcWrite(pwmChannelLeft, -l);
    digitalWrite(27, 1);
    digitalWrite(26, 0);
  } else {
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(27, 0);
    digitalWrite(26, 0);
  }
  if (r > 0){
    ledcWrite(pwmChannelRight, r);
    digitalWrite(25, 1);
    digitalWrite(33, 0);
  } else if (r < 0){
    ledcWrite(pwmChannelRight, -r);
    digitalWrite(25, 0);
    digitalWrite(33, 1);
  } else {
    ledcWrite(pwmChannelRight, r);
    digitalWrite(25, 0);
    digitalWrite(33, 0);
  }
}

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;

  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "--";

  if (httpResponseCode>0) {
    //Serial.print("HTTP Response code: ");
    //Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

int getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return (found > index ? data.substring(strIndex[0], strIndex[1]) : "").toInt();

}
