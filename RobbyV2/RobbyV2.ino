/*
  REALROBOTS.net
  Author: Jacob Wilkinson

  Robby v2 Receiver Firmware
  This code runs on the robot and requests input from Remote ESP Controller

  ssid must match ssid on remote
*/

#include <WiFi.h>
#include <HTTPClient.h>
#include <math.h>

#define left_en  14
#define left_a   27
#define left_b   26
#define right_a  25
#define right_b  33
#define right_en 32

#define sensor_left 5
#define sensor_right 4

#define DISABLE_MOTORS false
#define TESTMODE false
#define MODE_DIRECT_REMOTE 0
#define MODE_AUTO 1

int currentMode = MODE_DIRECT_REMOTE;

const char* ssid = "robby8";
const char* password = "123456789";

//Your IP address or domain name with URL path
const char* serverNameInputs = "http://192.168.4.1/inputs";


String inputString;
bool printValues = false;

int inputs[7];

long lastConnect = 0;
unsigned long previousMillis = 0;
const long interval = 100;

// setting PWM properties
const int freq = 5000;
const int pwmChannelLeft = 0;
const int pwmChannelRight = 1;
const int resolution = 8;

void setup() {
  Serial.begin(115200);

  //pinMode(left_en, OUTPUT);
  pinMode(left_a, OUTPUT);
  pinMode(left_b, OUTPUT);
  //pinMode(right_en, OUTPUT);
  pinMode(right_a, OUTPUT);
  pinMode(right_b, OUTPUT);

  pinMode(sensor_right, INPUT);
  pinMode(sensor_left, INPUT);
  // digitalWrite(right_en, 1);
  // digitalWrite(left_en, 1);

  ledcSetup(pwmChannelLeft, freq, resolution);
  ledcAttachPin(left_en, pwmChannelLeft);
  ledcSetup(pwmChannelRight, freq, resolution);
  ledcAttachPin(right_en, pwmChannelRight);

  ledcWrite(pwmChannelLeft, 0);
  ledcWrite(pwmChannelRight, 0);

  pinMode(2, OUTPUT);

  if (!TESTMODE) {
    connect();
  }
}

void connect() {
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

}

void loop() {
  unsigned long currentMillis = millis();

  if (TESTMODE){
    Serial.println("FORWARD");
    SetMotor(255, 255);
    delay(2000);
    Serial.println("REVERSE");
    SetMotor(-255, -255);
    delay(2000);
    Serial.println("STOP");
    SetMotor(0, 0);
    delay(2000);
    return;
  }

  if (currentMode == MODE_AUTO){
    AutoMode();
  }

  if (currentMillis - previousMillis >= interval) {
    // Check WiFi connection status
    if (WiFi.status() == WL_CONNECTED ) {
      inputString = httpGETRequest(serverNameInputs);
      if (printValues) Serial.println("InputString: " + inputString);
      //Serial.println(getValue(inputString, ',', 1));
      
      for (int i = 0; i < 7; i++) {
        inputs[i] = getValue(inputString, ',', i);
        if (printValues) {
          Serial.print(i);
          Serial.print(":");
          Serial.print(inputs[i]);
          Serial.print("\t");
        }
      }

      if (!inputs[3]){
        currentMode = MODE_AUTO;
        Serial.println("Auto Mode");
      }
      if (!inputs[4]){
        currentMode = MODE_DIRECT_REMOTE;
        Serial.println("Direct RC Mode");
      }

      if (currentMode == MODE_DIRECT_REMOTE){
        SetMotor(inputs[1], inputs[0]);
      }
      lastConnect = millis();


      digitalWrite(2, !digitalRead(2));
      if (printValues) Serial.println();
      // save the last HTTP GET Request
      previousMillis = currentMillis;
    }
    else {
      Serial.println("WiFi Disconnected");
      SetMotor(0, 0);
      connect();
    }
  }
}

void AutoMode(){
  if (LeftBlocked() && RightBlocked()){
    SetMotor(-255, 255);
    Serial.println("AUTO RIGHT");
  } else if (LeftBlocked()){
    SetMotor(-255, 255);
    Serial.println("AUTO RIGHT");
  } else if (RightBlocked()){
    SetMotor(255, -255);
    Serial.println("AUTO LEFT");
  } else {
    SetMotor(255, 255);
    Serial.println("AUTO FORWARD");
  }
}

bool LeftBlocked(){
  return !digitalRead(sensor_left);
}

bool RightBlocked(){
  return !digitalRead(sensor_right);
}

// Left and right motor -255 <-> 255
void SetMotor(int l, int r) {
  // Serial.print(l);
  // Serial.print("\t");
  // Serial.println(r);
  //return;

  if (DISABLE_MOTORS){
    l = 0;
    r = 0;
  }


  if (l > 0) {
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(left_a, 0);
    digitalWrite(left_b, 1);
  } else if (l < 0) {
    ledcWrite(pwmChannelLeft, -l);
    digitalWrite(left_a, 1);
    digitalWrite(left_b, 0);
  } else {
    ledcWrite(pwmChannelLeft, l);
    digitalWrite(left_a, 0);
    digitalWrite(left_b, 0);
  }
  if (r > 0) {
    ledcWrite(pwmChannelRight, r);
    digitalWrite(right_a, 1);
    digitalWrite(right_b, 0);
  } else if (r < 0) {
    ledcWrite(pwmChannelRight, -r);
    digitalWrite(right_a, 0);
    digitalWrite(right_b, 1);
  } else {
    ledcWrite(pwmChannelRight, r);
    digitalWrite(right_a, 0);
    digitalWrite(right_b, 0);
  }
}

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;

  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "--";

  if (httpResponseCode > 0) {
    //Serial.print("HTTP Response code: ");
    //Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

int getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return (found > index ? data.substring(strIndex[0], strIndex[1]) : "").toInt();

}
